<?php

/**
 * @file
 * Taxonomy functions.
 */

/**
 * Generate term id for a given vocabulary word.
 *
 * @param string $term_val
 *   Term value.
 * @param string $vocabulary_val
 *   Vocabulary name.
 *
 * @return int
 *    tid.
 */
function skyword_get_tid_for_term($term_val, $vocabulary_val) {
  $term       = taxonomy_get_term_by_name($term_val, $vocabulary_val);
  $vocabulary = taxonomy_vocabulary_machine_name_load($vocabulary_val);

  if ($vocabulary == NULL) {
    $vocabulary               = new stdClass();
    $vocabulary->name         = $vocabulary_val;
    $vocabulary->machine_name = $vocabulary_val;
    taxonomy_vocabulary_save($vocabulary);
  }

  if ($term == NULL) {
    $term              = new stdClass();
    $term->vid         = $vocabulary->vid;
    $term->name        = $term_val;
    $term->description = $term_val;
    taxonomy_term_save($term);
  }

  if (is_array($term)) {
    $term_array = array_keys($term);
    $tid        = $term_array['0'];
  }
  else {
    $tid = $term->tid;
  }
  return $tid;
}

/**
 * Pull vocabulary ids.
 *
 * @param string $hash
 *   Hashed key.
 * @param string $timestamp
 *   Timestamp.
 *
 * @return array
 *   Array of vocabularies.
 */
function skyword_get_vocabularies_callback($hash, $timestamp) {
  $test = skyword_validate_secret($hash, $timestamp);
  if (is_array($test)) {
    return $test;
  }

  $vocabularies = taxonomy_get_vocabularies();

  foreach ($vocabularies as &$vocabulary) {
    $count             = db_query("SELECT COUNT(tid) FROM {taxonomy_term_data} WHERE vid = :vid", array(':vid' => $vocabulary->vid))->fetchField();
    $vocabulary->count = $count;
  }

  return $vocabularies;
}

/**
 * Pull terms for vocabulary.
 *
 * @param string $hash
 *   Hashed key.
 * @param string $timestamp
 *   Timestamp.
 * @param int $vid
 *   Vocabulary id.
 *
 * @return array
 *   Array of terms.
 */
function skyword_get_vocabulary_terms_callback($hash, $timestamp, $vid) {
  $test = skyword_validate_secret($hash, $timestamp);
  if (is_array($test)) {
    return $test;
  }

  $taxonomy_tree = taxonomy_get_tree($vid);
  $terms         = array();
  foreach ($taxonomy_tree as $taxonomy) {
    $terms[$taxonomy->tid] = array(
      'tid'  => $taxonomy->tid,
      'name' => $taxonomy->name,
    );
  }

  return $terms;
}

/**
 * Pull taxonomies from system.
 *
 * @param string $hash
 *   Hashed key.
 * @param string $timestamp
 *   Timestamp.
 *
 * @return array
 *   Array of taxonomies.
 */
function skyword_get_taxonomies_callback($hash, $timestamp) {
  $test = skyword_validate_secret($hash, $timestamp);
  if (is_array($test)) {
    return $test;
  }
  else {
    $vocabulary_array = taxonomy_get_vocabularies();
    $taxonomies       = array();

    foreach ($vocabulary_array as $vocabulary) {
      $taxonomy_array = taxonomy_get_tree($vocabulary->vid, 0);
      $temp_array     = array();
      foreach ($taxonomy_array as $taxonomy) {
        $temp_array[] = array(
          'tid'  => $taxonomy->tid,
          'term' => $taxonomy->name,
        );
      }
      $taxonomies[$vocabulary->name . "||" . $vocabulary->machine_name] = $temp_array;
    }

    return $taxonomies;
  }
}

/**
 * Pull terms for vocabulary by chunking terms.
 *
 * @param string $hash
 *   Hashed key.
 * @param string $timestamp
 *   Timestamp.
 * @param int $vid
 *   Vocabulary id.
 * @param int $start
 *   First taxonomy to start at.
 * @param int $limit
 *   Amount of terms to get at a time.
 *
 * @return array
 *   Array of terms.
 */
function skyword_get_vocabulary_terms_chunk_callback($hash, $timestamp, $vid, $start, $limit) {
  $test = skyword_validate_secret($hash, $timestamp);
  if (is_array($test)) {
    return $test;
  }

  $query  = new EntityFieldQuery();
  $result = $query->entityCondition('entity_type', 'taxonomy_term')
                  ->propertyCondition('vid', $vid)
                  ->range($start, $limit)
                  ->execute();

  $tids = array_keys($result['taxonomy_term']);

  $loaded_terms = taxonomy_term_load_multiple($tids);

  $terms = array();
  foreach ($loaded_terms as $taxonomy) {
    $terms[$taxonomy->tid] = array(
      'tid'  => $taxonomy->tid,
      'name' => $taxonomy->name,
    );
  }

  return $terms;
}
